FROM archlinux:base-devel

LABEL version="0.4.0"
LABEL description="ArchLinux package repository image"
LABEL mainteiner="nhein@paradoxdev.com"

ARG PACKAGER="Nicholas R. Hein <nhein@paradoxdev.com>"
ARG TAREXT=tar.zst

ENV PKGEXT=.pkg.$TAREXT
ENV SRCEXT=.src.$TAREXT
ENV DBEXT=.db.$TAREXT
ENV PACKAGER=$PACKAGER
ENV AUR_REPO=custom
ENV AUR_DBROOT=/var/cache/pacman/$AUR_REPO
ENV AUR_BASE_URL=https://aur.archlinux.org
ENV AUR_SNAPSHOT_URL=$AUR_BASE_URL/cgit/aur.git/snapshot

RUN pacman-key --init && \
	pacman --sync --refresh && \
	pacman --sync --needed --noconfirm archlinux-keyring && \
	pacman --sync --needed --noconfirm ccache rsync subversion git python tree glab && \
	pacman --sync --sysupgrade --noconfirm && \
	useradd build --system --shell /bin/bash --home-dir /var/cache/build --create-home && \
	echo "build ALL=(root) NOPASSWD: ALL" > /etc/sudoers.d/build && \
	mkdir -p "$AUR_DBROOT" && \
	repo-add  --quiet "$AUR_DBROOT"/"$AUR_REPO""$DBEXT" && \
	chown -R build:build "$AUR_DBROOT" && \
	echo "[$AUR_REPO]" >> /etc/pacman.conf && \
	echo "SigLevel = Optional TrustAll" >> /etc/pacman.conf && \
	echo "Server = file://$AUR_DBROOT" >> /etc/pacman.conf && \
	sed -i "/^BUILDENV/s/!ccache/ccache/" /etc/makepkg.conf && \
	pacman --sync --refresh && \
	rm -rf /var/cache/pacman/pkg/*

USER build

WORKDIR /var/cache/build

RUN mkdir ~/.gnupg && \
	chmod 700 ~/.gnupg && \
	touch ~/.gnupg/gpg.conf && \
	echo "keyserver hkps://keys.openpgp.org" >> ~/.gnupg/gpg.conf && \
	echo "keyserver-options auto-key-retrieve" >> ~/.gnupg/gpg.conf && \
	echo "keyserver-options no-honor-keyserver-url" >> ~/.gnupg/gpg.conf && \
	gpg --recv-keys ED0044E23EA4E591 && \
	curl "$AUR_SNAPSHOT_URL"/aurutils.tar.gz --output aurutils.tar.gz && \
	tar -x -f aurutils.tar.gz && \
	cd aurutils && \
	makepkg --syncdeps --rmdeps --install --needed --noconfirm && \
	cd .. && \
	rm -f aurutils.tar.gz && \
	rm -rf aurutils && \
	yes | sudo pacman --sync --clean --clean

CMD ["/usr/bin/bash"]

# Credit to <https://gitlab.com/aur1/utils>
