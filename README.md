# Package Repository

Home Page: <https://nicholashein.gitlab.io/nicholashein-aur/>

## Packages

Package | License | Version
------- | ------- | -------
[`kmap-gis-data`](https://gitlab.com/kmap/gis-data) | ![](https://img.shields.io/gitlab/license/kmap/gis-data) | ![](https://img.shields.io/gitlab/v/release/kmap/gis-data)
[`kmap-gis-geotag`](https://gitlab.com/kmap/gis-geotag) | ![](https://img.shields.io/gitlab/license/kmap/gis-geotag) | ![](https://img.shields.io/gitlab/v/release/kmap/gis-geotag)
[`kmap-gis-server`](https://gitlab.com/kmap/gis-server) | ![](https://img.shields.io/gitlab/license/kmap/gis-server) | ![](https://img.shields.io/gitlab/v/release/kmap/gis-server)
[`kmap-kiosk`](https://gitlab.com/kmap/kiosk) | ![](https://img.shields.io/gitlab/license/kmap/kiosk) | ![](https://img.shields.io/gitlab/v/release/kmap/kiosk)
[`kmap-qwc2-app`](https://gitlab.com/kmap/qwc2-app) | ![](https://img.shields.io/gitlab/license/kmap/qwc2-app) | ![](https://img.shields.io/gitlab/v/release/kmap/qwc2-app)
[`nicholas-hein-resume`](https://gitlab.com/NicholasHein/resume) | ![](https://img.shields.io/gitlab/license/NicholasHein/resume) | ![](https://img.shields.io/gitlab/v/release/NicholasHein/resume)
[`server-status`](https://gitlab.com/NicholasHein/server-status) | ![](https://img.shields.io/gitlab/license/NicholasHein/server-status) | ![](https://img.shields.io/gitlab/v/release/NicholasHein/server-status)

## Usage

Add the following code to `/etc/pacman.conf`:

```toml
[nicholashein-aur]
SigLevel = PackageOptional
Server = https://nicholashein.gitlab.io/$repo
```

Then run:

```bash
pacman -Syy
```

## Credit

Credit to <https://gitlab.com/aur1/utils>.
